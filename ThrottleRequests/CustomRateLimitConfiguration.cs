﻿using AspNetCoreRateLimit;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace ThrottleRequests
{
    public class CustomRateLimitConfiguration : RateLimitConfiguration
    {
        public CustomRateLimitConfiguration(IHttpContextAccessor httpContextAccessor, IOptions<IpRateLimitOptions> ipOptions, IOptions<ClientRateLimitOptions> clientOptions) : base(httpContextAccessor, ipOptions, clientOptions)
        {
        }

        protected override void RegisterResolvers()
        {
            ClientResolvers.Add(new IpAndAgentClientResolveContributor(HttpContextAccessor));
        }
    }

    public class IpAndAgentClientResolveContributor : IClientResolveContributor
    {
        private IHttpContextAccessor _httpContextAccessor;

        public IpAndAgentClientResolveContributor(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string ResolveClient()
        {
            var remoteIpAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress;
            var userAgent = _httpContextAccessor.HttpContext.Request.Headers["User-Agent"].ToString();
            return $"client-[IP: {remoteIpAddress}; USERAGENT: {userAgent}]";
        }
    }


}
